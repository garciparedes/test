#!/usr/bin/env python3
from subprocess import (
    check_output,
    call,
)
from json import load
from pathlib import Path


def main():
    repository_path = Path.cwd()

    config_file_path = repository_path / 'lib' / 'config.json'
    print(config_file_path.absolute())
    if not config_file_path.exists():
        return

    with config_file_path.open() as file:
        config = load(file)

    project_name = repository_path.absolute().name
    branch_name = check_output(['git', 'rev-parse', '--abbrev-ref', 'HEAD'], cwd=str(repository_path)).decode().strip()
    print(branch_name)
    for subtree in config['subtrees']:
        subtree_directory_path = repository_path / 'lib' / subtree['name']
        if not subtree_directory_path.exists():
            command = ['git', 'subtree', 'add', '--prefix', f'lib/{subtree["name"]}', f'{subtree["url"]}', 'master']
            print(f'COMMAND: "{" ".join(command)}"')
            call(command, cwd=str(repository_path))
        else:
            command = ['git', 'subtree', 'pull', '--prefix', f'lib/{subtree["name"]}', f'{subtree["url"]}', 'master']
            print(f'COMMAND: "{" ".join(command)}"')
            call(command, cwd=str(repository_path))

        command = [
            f'git', 'subtree', 'push',
            '--prefix', f'lib/{subtree["name"]}',
            f'{subtree["url"]}', f'{branch_name}-{project_name}'
        ]
        print(f'COMMAND: "{" ".join(command)}"')
        call(command, cwd=str(repository_path))


if __name__ == '__main__':
    main()
